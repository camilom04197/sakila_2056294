<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use Illuminate\Support\Facades\Validator;

class CategoriaController extends Controller{
    //Accion: un metodo del controlador, contienen el codigo sa ejecutar
    //Nombre: puede ser cualquiera
    //RECOMENDADO EL NOMBRE WN MINUSCULA
    public function index(){
        //Accion:UN METODO DEL CONTROLLADOR, contienen el codigo a ejecutar
        //Nombre: puede ser cualquiera
        //recomendado el nombre en minuscula
        //seleccionar las categorias existentes
        $categorias = Categoria::paginate(5);
        //enviar la coleccion de categorias a una vista
        //y las vamos a mostrar alli
        return view("categorias.index")->with("categorias" , $categorias);

    }
    //Metodo create:mostrar el formulario de crear categoria
    public function create(){
        //echo "Formulario de categoria";
        return view('categorias.new');
    }
    //llegar los datos desde el formulario
    //guardar la categoria en BD
    public function store(Request $r){
        //1 establecer las reglas de validacion para cada campo
        $reglas = [
            "categoria" =>["required", "alpha"]
        ];
        $mensaje =[
            "required"=> "Campo obligatorio",
            "alpha" => "Solo letras"
        ];

        //2.crear objeto validador
        $validador = Validator::make($r->all(), $reglas, $reglas, $mensaje);
        //3. validar: metodo validador
        //            retorna true(v) si la validacion falla
        //             retorna falso en caso de que los datos sena correctos
        if($validador->fails()){
            //Codigo cuando falla la validacion
            return redirect("categorias/create")->withErrors($validador);
        }else{
            //codigo cuando la validacion es correcta
        }

        //$_POST= arreglo php
        //almacena la informacion que viene de los formularios 
        //var_dump($_POST);
        //crear nueva categoria
        $categoria = new Categoria();
        //asignar nombre
        $categoria->name = $r ->input("categoria");
        //guardar la nueva categoria
        $categoria->save();
        //letrero de exito
        return redirect('categorias/create')->with('mensaje','Categoria guardada');
        

    }
    public function edit($category_id) {
        $categoria = Categoria::find($category_id);
        return view("categorias.edit")->with("categoria", $categoria);
    }
    public function update($category_id){
        //seleccionar la categoria a editar
        $categoria=Categoria::find($category_id);
        //editarr sus atributos
        $categoria->name = $_POST["categoria"];
        //guardar cambios
        $categoria->save();
        //retornar formulario de edit
        return redirect("categorias/edit/$category_id")->with("mensaje", "Categoria Editada");
    }

}